import Arrow from './arrows/genericArrow';
import { determineFontColor } from '../functions';

const Indicator = ({ title, subtitle, rotation, backgroundColor = '#000000', borderRadius, font, arrowType }) => {
	const color = determineFontColor( backgroundColor );
	const style = {
		backgroundColor,
		borderRadius: borderRadius,
		maxHeight: '6em',
		color: color,
	};
	return <div className={ 'indicator' } style={ style }>
		<div className={ 'left' }>
			<div className={ 'metric' } style={ { fontFamily: font } }> { title } </div>
			<div className={ 'subtitle' } style={ { fontFamily: font } }>{ subtitle }</div>
		</div>
		<div className={ 'right' }>
			<Arrow type={ arrowType } rotation={ rotation } color={ color } />
		</div>
	</div>; 
};

export default Indicator;
