import IndicatorArrow from './EditableIndicatorArrow';
import EditableParagraph from './EditableParagraph';
import { darkColor, determineFontColor } from '../functions';

const IndicatorEditor = ({ title = '', subtitle = '', rotation, background = '#000000', borderRadius, onChange, font, arrowType }) => {
	const color = determineFontColor( background );

	const style = {
		backgroundColor: background,
		borderRadius: borderRadius,
		maxHeight: '6em',
		fontFamily: font,
		color,
	};

	return <div className={ 'indicator' } style={ style }>
		<div className={ 'left' }>
			<EditableParagraph
				text={ title }
				style={ { color, fontFamily: font } }
				onChange={ onChange( 'title' ) }
				className="metric"
				placeholder="Add title"
				dark={ color === darkColor } />
			<EditableParagraph
				text={ subtitle }
				style={ { color, fontFamily: font } }
				className={ 'subtitle' }
				onChange={ onChange( 'subtitle' ) }
				placeholder="Add subtitle" />
		</div>
		<div className={ 'right' }>
			<IndicatorArrow arrowType={ arrowType } rotation={ rotation } editable color={ color } onChange={ onChange( 'rotation' ) } />
		</div>
	</div>;
};

export default IndicatorEditor;
