import React from 'react';

const RotatableIndicatorArrow = ({ onClick, color = '#ffffff' }) => {
	return <button
		onClick={ onClick }
		style={ { color } }
		className="rotateIndicatorArrow">
		<span className="dashicons dashicons-image-rotate"></span>
	</button>;
};

export default RotatableIndicatorArrow;
