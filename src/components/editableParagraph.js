const EditableParagraph = ({ text, onChange, className, dark, style, placeholder }) => {
	return ( <input
		type="text"
		className={ `${ className } editableParagraph ${ dark ? 'dark' : 'light' }` }
		value={ text }
		onChange={ ( e ) => {
			onChange( e.target.value );
		} }
		style={ style }
		placeholder={ placeholder } />
	);
};

export default EditableParagraph;
