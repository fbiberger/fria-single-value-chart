import React, { Fragment } from 'react';
import RotatableIndicatorArrow from './RotatableIndicatorArrow';
import Arrow from './arrows/genericArrow';

const EditableIndicatorArrow = ({ rotation = 0, onChange, color, arrowType }) => {
	const handleClick = ( ) => {
		const newRotation = ( rotation + 45 ) % 360;
		onChange( newRotation );
	};

	return (
		<Fragment>
			<Arrow type={ arrowType } rotation={ rotation } color={ color } />
			<RotatableIndicatorArrow onClick={ handleClick } color={ color } />
		</Fragment>
	);
};

export default EditableIndicatorArrow;
