import React from 'react';
import SimpleArrow from './simple';
import ThinArrow from './thin';
import Trendline from './trendline';
import { __ } from '@wordpress/i18n';

export const arrows = {
	thin: {
		component: ThinArrow,
		label: __( 'Thin' ),
		key: 'thin',
	},
	simple: {
		component: SimpleArrow,
		label: __( 'Simple' ),
		key: 'simple',
	},
	trendline: {
		component: Trendline,
		label: __( 'Trendline' ),
		key: 'trendline',
	},
};

export const Arrow = ({ type = 'simple', rotation, color }) => {
	return React.createElement( arrows[ type ].component, { rotation, color });
};

export default Arrow;
