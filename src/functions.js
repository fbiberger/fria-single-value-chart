export const lightColor = '#eeeeee';
export const darkColor = '#333333';

export const determineFontColor = ( backgroundColor ) => {
	if ( backgroundColor.length === 7 ) {
		return [
			backgroundColor[ 1 ] + backgroundColor[ 2 ],
			backgroundColor[ 3 ] + backgroundColor[ 4 ],
			backgroundColor[ 5 ] + backgroundColor[ 6 ],
		].reduce( ( acc, item ) => {
			return acc + parseInt( item, 16 );
		}, 0 ) > 396 ? darkColor : lightColor;
	}
	return darkColor;
};
