/**
 * BLOCK: wp-fria-single-value-chart
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import IndicatorEditor from '../components/IndicatorEditor';
import Indicator from '../components/RenderIndicator';
import SidebarComponents from './SidebarComponents';
const { __ } = wp.i18n;
const { withColors } = wp.editor;
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

registerBlockType( 'fria/single-value-chart', {
	title: __( 'FRI‧A Single Value Chart' ),
	icon: 'arrow-up-alt',
	category: 'common',
	keywords: [
		__( 'FRI‧A Single Value Chart' ),
		__( 'Metric' ),
		__( 'KPI' ),
		__( 'Chart' ),
		__( 'Single Value' ),
	],
	attributes: {
		config: {
			type: 'object',
		},
		backgroundColor: {
			type: 'string',
			default: 'inherit',
		},
	},

	edit: withColors( 'backgroundColor' )( ({ className, attributes, setAttributes, backgroundColor, setBackgroundColor }) => {
		const handleChange = ( id ) => ( content ) => {
			setAttributes({
				config: {
					...attributes.config,
					[ id ]: content,
				},
			});
		};
		return (
			<div className={ className }>
				<IndicatorEditor onChange={ handleChange } { ...attributes.config } background={ backgroundColor.color } />
				<SidebarComponents backgroundColor={ backgroundColor } setBackgroundColor={ setBackgroundColor } config={ attributes.config } onChange={ handleChange } />
			</div>
		);
	}),

	save: ({ attributes, className }) => {
		return (
			<div className={ className }>
				<Indicator { ...attributes.config } />
			</div>
		);
	},
});
