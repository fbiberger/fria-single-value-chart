import { arrows } from '../components/arrows/genericArrow';

const { InspectorControls, PanelColorSettings } = wp.blockEditor;
const { TextControl, PanelBody, SelectControl } = wp.components;
const { __ } = wp.i18n;

export const SidebarComponents = ({ backgroundColor, setBackgroundColor, onChange, config }) => {
	return (
		<InspectorControls>
			<PanelBody title={ __( 'Configure FRI‧A Indicator' ) }>
				<PanelColorSettings
					title={ __( 'Color Settings' ) }
					colorSettings={ [
						{
							value: backgroundColor.color,
							onChange: ( c ) => {
								onChange( 'backgroundColor' )( c );
								setBackgroundColor( c );
							},
							label: __( 'Background Color' ),
						},
					] }
				/>
				<h4> { __( 'Font Settings' ) } </h4>
				<TextControl
					value={ config ? config.font : 'inherit' }
					label={ __( 'Set font (default is inherit)' ) }
					onChange={ onChange( 'font' ) }
				/>
				<h4> { __( 'Arrow Type' ) }</h4>
				<SelectControl
					label={ __( 'Select Arrow Type' ) }
					value={ config ? config.arrowType : 'simple' }
					options={ Object.values( arrows ).map( arrow => ({ label: arrow.label, value: arrow.key }) ) }
					onChange={ onChange( 'arrowType' ) }
				/>
			</PanelBody>
		</InspectorControls>
	);
};

export default SidebarComponents;
