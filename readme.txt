=== FRIA Single Value Chart ===
Contributors: fbiberger
Tags: charts, gutenberg, block, single value, arrow
Requires at least: 5.3
Tested up to: 5.5
Requires PHP: 7.3
License: GPL2+
License URI: https://www.gnu.org/licenses/gpl-2.0.txt

A simple plugin that displays a badge with a text and an arrow. The purpose is to display trends and business metrics on your wordpress site.

== Description ==
FRI‧A Single Value Chart displays a simple badge with a value, a text and an arrow. The purpose is to display trends and business metrics on your wordpress site.

It allows you to change colors, arrows and fonts used to display the badge.

The badge is only available as Gutenberg-Block and you will find it in the Gutenberg Editor when using it.

== Installation ==
1. Visit "Plugins > Add New"
2. Search for "FRIA Single Value Chart"
3. Install "FRIA Single Value Chart" once it appears
4. Activate "FRIA Single Value Chart" from your Plugins page.

=Manually=
1. Upload the fria-single-value-chart folder to the /wp-content/plugins/ directory
2. Activate "FRIA Single Value Chart" from your Plugins page.

=After activation=
You should find the \"FRIA Single Value Chart\"- block in the Gutenberg Blocks selection.

== Frequently Asked Questions ==
Actually there are no FAQs yet. If you have any questions, feel free to open an issue here: https://gitlab.com/fbiberger/fria-single-value-chart/-/issues

==Screenshots== 
1. Inline edit the value and subtitle
2. Change color, font and arrowType in Gutenberg sidebar
3. Change arrow orientation

== Changelog ==
=Version 1.0.1=
* Renamed the blocks - sorry for that, but the directory did not show them correctly
* Fixed the case where the rotation UI was not shown and repositioned it

=Version 1.0=
* Change arrowType and orientation orientation
* Change color
* Change value und subtitle
* Change font