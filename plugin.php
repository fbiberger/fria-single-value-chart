<?php
/**
 * Plugin Name: FRI‧A Single Value Chart
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: A simple Gutenberg block plugin that displays a badge with a text and an arrow. The purpose is to display trends and business metrics on your wordpress site.
 * Author: Fabian Biberger
 * Author URI: https://fbiberger.de/
 * Version: {{VERSION}}
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
