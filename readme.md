## What does this plugin do?
This plugin is a Gutenberg-Block that displays a basic single value chart for a KPI or metric. It is very basic and has the following features at the moment:
- Change Metric and Subtitle
- Change rotation for the Arrow
- Change background color (foreground color is adjusted automatically)
- Change font for the chart
- Select one of three different arrow types

![Indicator Example](./assets/Indicator.png)

### Unconfigurable parts
- Size (automatically adjusted, max-width 400px, min-height 90px);

### Roadmap
- Adjust font sizes
- Adjust sizing
- Add your own arrow-images
- Adjust background shadow
- Adjust foreground colors

## Installation
- Easiest way to get this plugin is from Wordpress.org plugin directory
- But you could also:

### Install manually

### Development
- Clone this repository into the plugin folder of your wordpress installation
- `cd {your-installation-path}/wp-content/plugins/fria-single-value-chart`
- `npm install`

#### 👉  `npm start`
- Use to compile and run the block in development mode.
- Watches for any changes and reports back any errors in your code.

#### 👉  `npm run build`
- Use to build production code for your block inside `dist` folder.
- Runs once and reports back the gzip file sizes of the produced code.

#### 👉  `npm test`
- Runs the tests

### Contribution
Feel free to contribute to this project by writing bug reports, filing issues and creating merge requests.

Before contributing, please check the [Code of Conduct](./code_of_conduct.md)

## Credits
This project was bootstrapped with [Create Guten Block](https://github.com/ahmadawais/create-guten-block).

* Thin Arrow from https://dribbble.com/shots/1182482-budicon-tester via http://svgicons.sparkk.fr/
* Simple and Trendline from https://github.com/akveo/eva-icons

## Copyright Notice
FRIA Single Value Chart - A simple chart displaying a single value for the Gutenberg Editor

Copyright (C) 2020 fbiberger

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.