// Link.react.test.js
import React from 'react';
import renderer from 'react-test-renderer';
import RotateIndicatorArrow from '../src/components/RotatableIndicatorArrow';

test( 'afterRender onClick event is callable', () => {
	const mockHandleClick = jest.fn();

	const renderedComponent = renderer.create(
		<RotateIndicatorArrow onClick={ mockHandleClick } />,
	);

	renderedComponent.toJSON().props.onClick();

	expect( mockHandleClick.mock.calls ).toHaveLength( 1 );
});

test( 'afterRender color is set properly (snapshot)', () => {
	const renderedComponent = renderer.create(
		<RotateIndicatorArrow color={ '#ff0000' } />,
	);

	const tree = renderedComponent.toJSON();
	expect( tree ).toMatchSnapshot();
});

test( 'afterRender default color is set to white (snapshot)', () => {
	const renderedComponent = renderer.create(
		<RotateIndicatorArrow />,
	);

	const tree = renderedComponent.toJSON();
	expect( tree ).toMatchSnapshot();
});
