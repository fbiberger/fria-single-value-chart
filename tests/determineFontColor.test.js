import { determineFontColor, darkColor, lightColor } from '../src/functions';

test( 'determineFontColor returns darkColor for a light background', () => {
	expect( determineFontColor( '#ffffff' ) ).toBe( darkColor );
});

test( 'determineFontColor returns lightColor for a dark background', () => {
	expect( determineFontColor( '#000000' ) ).toBe( lightColor );
});

test( 'determineFontColor returns lightColor for an invalid font value', () => {
	expect( determineFontColor( '#fffff' ) ).toBe( darkColor );
});
