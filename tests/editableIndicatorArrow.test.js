import React from 'react';
import renderer from 'react-test-renderer';
import EditableIndicatorArrow from '../src/components/EditableIndicatorArrow';

test( 'onClick calls the the onChange function with 45 degree increase', () => {
	const mockHandleClick = jest.fn();

	const renderedComponent = renderer.create(
		<EditableIndicatorArrow rotation={ 0 } onChange={ mockHandleClick } />,
	);

	const tree = renderedComponent.toJSON();
	tree.filter( child => ( child.type === 'button' ) )[ 0 ].props.onClick();

	expect( mockHandleClick ).toHaveBeenCalledWith( 45 );
});
